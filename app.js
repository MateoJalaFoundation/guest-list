//Add guest to the list
function addGuest() {
    var li = document.createElement("li");
    var nameInputValue = document.getElementById("nameInput").value;
    var lastNameInputValue = document.getElementById("lastNameInput").value;
    var fullNameText = document.createTextNode(nameInputValue+" "+lastNameInputValue);
    li.appendChild(fullNameText);
  
    if (nameInputValue === "" || lastNameInputValue === "") {
      alert("You must provide the guest name");
    } else {
      document.getElementById("guestList").appendChild(li);
    }
    
    document.getElementById("nameInput").value = "";
    document.getElementById("lastNameInput").value = "";
  }
  
  //Clear the guest list
  function clearList(){
    var lst = document.getElementsByTagName("ul");
      lst[0].innerHTML = "";
  }